module AminoAcids exposing
    ( AminoAcids
    , add
    , balance
    , completeness
    , decoder
    )

import Json.Decode as Decode exposing (Decoder)
import Json.Decode.Pipeline as Decode


type alias AminoAcids =
    { histidine : Float
    , isoleucine : Float
    , leucine : Float
    , lysine : Float
    , methionine : Float
    , phenylalanine : Float
    , threonine : Float
    , tryptophan : Float
    , valine : Float
    }


add : AminoAcids -> AminoAcids -> AminoAcids
add a b =
    { histidine = a.histidine + b.histidine
    , isoleucine = a.isoleucine + b.isoleucine
    , leucine = a.leucine + b.leucine
    , lysine = a.lysine + b.lysine
    , methionine = a.methionine + b.methionine
    , phenylalanine = a.phenylalanine + b.phenylalanine
    , threonine = a.threonine + b.threonine
    , tryptophan = a.tryptophan + b.tryptophan
    , valine = a.valine + b.valine
    }


{-| Optimal proportions

TODO: This numbers come from several (?) papers found by Dan, but we don't know where they got them from. Dan is on it.

-}
proportions : AminoAcids
proportions =
    { histidine = 1.08
    , isoleucine = 1
    , leucine = 1.87
    , lysine = 1.55
    , methionine = 0.32
    , phenylalanine = 1.55
    , threonine = 1.47
    , tryptophan = 0.5
    , valine = 1.17
    }


balance : AminoAcids -> Float
balance aminoacids =
    let
        amounts =
            [ aminoacids.histidine / proportions.histidine
            , aminoacids.isoleucine / proportions.isoleucine
            , aminoacids.leucine / proportions.leucine
            , aminoacids.lysine / proportions.lysine
            , aminoacids.methionine / proportions.methionine
            , aminoacids.phenylalanine / proportions.phenylalanine
            , aminoacids.threonine / proportions.threonine
            , aminoacids.tryptophan / proportions.tryptophan
            , aminoacids.valine / proportions.valine
            ]

        average =
            sum / length

        minimum =
            amounts
                |> List.minimum
                |> Maybe.withDefault 0

        sum =
            List.sum amounts

        length =
            toFloat (List.length amounts)
    in
    (average - minimum) / average


completeness : AminoAcids -> Float
completeness aminoacids =
    let
        amounts =
            [ aminoacids.histidine / proportions.histidine
            , aminoacids.isoleucine / proportions.isoleucine
            , aminoacids.leucine / proportions.leucine
            , aminoacids.lysine / proportions.lysine
            , aminoacids.methionine / proportions.methionine
            , aminoacids.phenylalanine / proportions.phenylalanine
            , aminoacids.threonine / proportions.threonine
            , aminoacids.tryptophan / proportions.tryptophan
            , aminoacids.valine / proportions.valine
            ]

        minimum =
            amounts
                |> List.minimum
                |> Maybe.withDefault 0
    in
    minimum


decoder : Decoder AminoAcids
decoder =
    Decode.succeed AminoAcids
        |> Decode.required "histidine" Decode.float
        |> Decode.required "isoleucine" Decode.float
        |> Decode.required "leucine" Decode.float
        |> Decode.required "lysine" Decode.float
        |> Decode.required "methionine" Decode.float
        |> Decode.required "phenylalanine" Decode.float
        |> Decode.required "threonine" Decode.float
        |> Decode.required "tryptophan" Decode.float
        |> Decode.required "valine" Decode.float
