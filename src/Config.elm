module Config exposing (Config, decoder)

import Food exposing (Food)
import Json.Decode as Decode exposing (Decoder)


type alias Config =
    { foods : List Food }


decoder : Decoder Config
decoder =
    Decode.map
        Config
        (Decode.field "foods"
            (Decode.list Food.decoder)
        )
