module Main exposing (main)

import Browser
import Config
import Element exposing (Element)
import Element.Font as Font
import Element.Input as Input
import Food exposing (Food)
import Html exposing (Html)
import Json.Decode as Decode
import List.Extra as List
import Result.Extra as Result


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Flags =
    { config : Decode.Value
    }


type alias Model =
    { foods : List Food
    , plate : List Food
    , filter : String
    }


type Msg
    = NoOp
    | FilterInputChanged String
    | AddToPlate Food
    | RemoveFromPlate Food


init : Flags -> ( Model, Cmd Msg )
init flags =
    let
        foods =
            flags.config
                |> Decode.decodeValue Config.decoder
                |> Result.map .foods
                -- TODO: Handle incorrect config
                |> Result.withDefault []
    in
    ( { foods = foods
      , filter = ""
      , plate = []
      }
    , Cmd.none
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )

        FilterInputChanged value ->
            ( { model | filter = value }
            , Cmd.none
            )

        AddToPlate food ->
            ( { model
                | plate = food :: model.plate
              }
            , Cmd.none
            )

        RemoveFromPlate food ->
            ( { model
                | plate =
                    model.plate |> List.filter ((/=) food)
              }
            , Cmd.none
            )


view : Model -> Html Msg
view model =
    model
        |> ui
        |> Element.layout
            [ Element.width Element.fill
            , Element.height Element.fill
            ]


ui : Model -> Element Msg
ui model =
    [ plateUi model.plate
    , balanceUi model.plate
    , completenessUi model.plate
    , filterUi model.filter
    , optionsUi model.filter model.plate model.foods
    ]
        |> Element.column []


plateUi : List Food -> Element Msg
plateUi foods =
    case foods of
        [] ->
            headingUi "You have nothing on your plate"
                |> Element.el [ Element.padding 20 ]

        _ ->
            foods
                |> List.map (foodItemUi RemoveFromPlate)
                |> (::) (headingUi "On your plate")
                |> Element.column
                    [ Element.spacing 20
                    , Element.padding 20
                    ]


completenessUi : List Food -> Element Msg
completenessUi foods =
    foods
        |> Food.completeness
        |> Maybe.map String.fromFloat
        |> Maybe.map ((++) "Completeness: ")
        |> Maybe.withDefault "Add some food to see the completetness"
        |> Element.text
        |> Element.el
            [ Element.padding 20
            , Font.bold
            ]


balanceUi : List Food -> Element Msg
balanceUi foods =
    foods
        |> Food.balance
        |> Maybe.map String.fromFloat
        |> Maybe.map ((++) "Balance: ")
        |> Maybe.withDefault "Add some food to see the balance"
        |> Element.text
        |> Element.el
            [ Element.padding 20
            , Font.bold
            ]


filterUi : String -> Element Msg
filterUi value =
    Input.text []
        { onChange = FilterInputChanged
        , text = value
        , placeholder = Nothing
        , label = Input.labelHidden "Filter"
        }
        |> Element.el [ Element.padding 20 ]


optionsUi : String -> List Food -> List Food -> Element Msg
optionsUi filter plate foods =
    let
        matchesName : Food -> Bool
        matchesName food =
            food.name
                |> String.toLower
                |> String.contains (String.toLower filter)

        isOnPlate : Food -> Bool
        isOnPlate food =
            not (List.member food plate)

        plateBalancing : Food -> Float
        plateBalancing food =
            food
                :: plate
                |> Food.balance
                |> Maybe.withDefault 0

        plateCompleteness : Food -> Float
        plateCompleteness food =
            food
                :: plate
                |> Food.completeness
                |> Maybe.withDefault 0
    in
    foods
        |> List.filter matchesName
        |> List.filter isOnPlate
        |> List.sortBy plateCompleteness
        |> List.reverse
        |> List.map (foodItemUi AddToPlate)
        |> (::) (headingUi "How about adding ")
        |> Element.column
            [ Element.spacing 20
            , Element.padding 20
            ]


headingUi : String -> Element Msg
headingUi text =
    text
        |> Element.text
        |> Element.el [ Font.bold ]


foodItemUi : (Food -> Msg) -> Food -> Element Msg
foodItemUi callback food =
    Input.button
        []
        { onPress = food |> callback |> Just
        , label = Element.text food.name
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none
