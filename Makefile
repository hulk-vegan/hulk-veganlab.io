.PHONY: all
all: .installed lint test dist

.PHONY: install
install:
	rm -rf .installed
	@make .installed

.installed: package-lock.json
	@echo "Dependencies files are newer than .installed; (re)installing."
	npm clean-install
	@echo "This file is used by 'make' for keeping track of last install time. If package-lock.json file is newer then this file (.installed) then all 'make *' commands that depend on '.installed' know they need to run npm install first." \
		> .installed

# Testing and linting targets
.PHONY: lint
lint: .installed
	npx elm-analyse

.PHONY: test
test: tests

.PHONY: tests
tests: .installed coverage

.PHONY: coverage
coverage: .installed verify-examples
	npx elm-coverage --report codecov

.PHONY: verify-examples
verify-examples: .installed
	npx elm-verify-examples

.coverage/codecov.json: .installed test

# Run development server
.PHONY: run
run: .installed
	npx parcel src/index.html

.PHONY: codecov
codecov: .coverage/codecov.json
	npx codecov --disable=gcov --file=.coverage/codecov.json

# Build distribution files and place them where they are expected
.PHONY: dist
dist: .installed config.json
	npx parcel build src/index.html

# The foods.json file is an output of Jupyter Notebook
# TODO: Check the source data files and the Jupyter Notebook into the repository
config.json: foods.json
	jq '{ "foods": . }' foods.json > config.json

clean:
	rm -rf elm-stuff/ dist/ node_modules/ .installed

